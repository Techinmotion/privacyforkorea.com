# 3 Apps to Consider When Working From Home #

So, they have offered you the chance to work from home and are quite excited by it. No more commuting or a boss leaning over your shoulder. This is a great way to work. Still, make sure you have the tools necessary to get that work done effectively. 

For starters, you will need to consider getting:

## Collaboration Apps ##

If you are working as part of a team, you cannot bend over to ask them a question as you have when in the office. To do this, and to perform all other tasks as a team, you will need collaboration apps such as Slack, Google Drive, Google Sheets, Word365, and Trello.
These all allow you to stay connected, organized, and structured so that projects can get done effortlessly.  

## Video Conferencing Apps  ##

For those times when you need a team meeting or conference, you are going to need a video conferencing app. The best one right now is Zoom, but Skype has always been the biggest name. You can all turn on your webcams and engage in video and voice chat. Just make sure you have got dressed for the day, working in your underwear is fine but attending meetings is not.

## VPN Apps ##

Working from home means that you will handle sensitive company data. This data may be valuable to hackers out there, so you will want to make it as difficult for them to steal it as possible. Connecting to a virtual private network is the best way to do this. A VPN encrypts your connection and the data it sends, making it impossible for hackers to get up to their old tricks.

[https://privacyforkorea.com](https://privacyforkorea.com)